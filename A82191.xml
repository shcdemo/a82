<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A82191">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The declaration of the navie, being the true copie of a letter from the officers of the navie, to the commissioners vvith their resolutions upon turning out Colonell Rainsbrough from being their commander. 28th. May, 1648.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A82191 of text R210803 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.12[36]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A82191</idno>
    <idno type="STC">Wing D713</idno>
    <idno type="STC">Thomason 669.f.12[36]</idno>
    <idno type="STC">ESTC R210803</idno>
    <idno type="EEBO-CITATION">99869560</idno>
    <idno type="PROQUEST">99869560</idno>
    <idno type="VID">162829</idno>
    <idno type="PROQUESTGOID">2248506827</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A82191)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162829)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f12[36])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The declaration of the navie, being the true copie of a letter from the officers of the navie, to the commissioners vvith their resolutions upon turning out Colonell Rainsbrough from being their commander. 28th. May, 1648.</title>
      <author>Lisle, George, Sir, d. 1648.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1648]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed at end: Thomas Lisle [and 8 others, plus the "Captaine of the Roebuck, Hinde, and severall other Officers of these and other Ships."].</note>
      <note>Imprint from Wing.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Rainborow, Thomas, d. 1648.</term>
     <term>Great Britain -- History, Naval -- 17th century -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A82191</ep:tcp>
    <ep:estc> R210803</ep:estc>
    <ep:stc> (Thomason 669.f.12[36]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>The declaration of the navie, being the true copie of a letter from the officers of the navie, to the commissioners: vvith their resolutions</ep:title>
    <ep:author>Lisle, George, Sir</ep:author>
    <ep:publicationYear>1648</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>392</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-10</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-10</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2007-11</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A82191-e10">
  <body xml:id="A82191-e20">
   <pb facs="tcp:162829:1" rend="simple:additions" xml:id="A82191-001-a"/>
   <div type="text" xml:id="A82191-e30">
    <head xml:id="A82191-e40">
     <w lemma="the" pos="d" xml:id="A82191-001-a-0010">THE</w>
     <w lemma="declaration" pos="n1" xml:id="A82191-001-a-0020">DECLARATION</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0030">Of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0040">the</w>
     <w lemma="navy" pos="n1" reg="navy" xml:id="A82191-001-a-0050">Navie</w>
     <pc xml:id="A82191-001-a-0060">,</pc>
     <w lemma="be" pos="vvg" xml:id="A82191-001-a-0070">being</w>
     <hi xml:id="A82191-e50">
      <w lemma="the" pos="d" xml:id="A82191-001-a-0080">THE</w>
     </hi>
     <w lemma="true" pos="j" xml:id="A82191-001-a-0090">True</w>
     <w lemma="copy" pos="n1" reg="copy" xml:id="A82191-001-a-0100">Copie</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0110">of</w>
     <w lemma="a" pos="d" xml:id="A82191-001-a-0120">a</w>
     <w lemma="letter" pos="n1" xml:id="A82191-001-a-0130">Letter</w>
     <w lemma="from" pos="acp" xml:id="A82191-001-a-0140">from</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0150">the</w>
     <w lemma="officer" pos="n2" xml:id="A82191-001-a-0160">Officers</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0170">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0180">the</w>
     <w lemma="navy" pos="n1" reg="navy" xml:id="A82191-001-a-0190">Navie</w>
     <pc xml:id="A82191-001-a-0200">,</pc>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-0210">to</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0220">the</w>
     <hi xml:id="A82191-e60">
      <w lemma="commissioner" pos="n2" xml:id="A82191-001-a-0230">Commissioners</w>
      <pc xml:id="A82191-001-a-0240">:</pc>
     </hi>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-0250">With</w>
     <w lemma="their" pos="po" xml:id="A82191-001-a-0260">their</w>
     <w lemma="resolution" pos="n2" xml:id="A82191-001-a-0270">Resolutions</w>
     <w lemma="upon" pos="acp" xml:id="A82191-001-a-0280">upon</w>
     <w lemma="turn" pos="vvg" xml:id="A82191-001-a-0290">turning</w>
     <w lemma="out" pos="av" xml:id="A82191-001-a-0300">out</w>
     <w lemma="colonel" pos="n1" reg="colonel" xml:id="A82191-001-a-0310">Colonell</w>
     <w lemma="RAINSBROUGH" pos="nn1" xml:id="A82191-001-a-0320">RAINSBROUGH</w>
     <w lemma="from" pos="acp" xml:id="A82191-001-a-0330">from</w>
     <w lemma="be" pos="vvg" xml:id="A82191-001-a-0340">being</w>
     <w lemma="their" pos="po" xml:id="A82191-001-a-0350">their</w>
     <w lemma="commander" pos="n1" xml:id="A82191-001-a-0360">Commander</w>
     <pc unit="sentence" xml:id="A82191-001-a-0370">.</pc>
    </head>
    <head xml:id="A82191-e70">
     <date xml:id="A82191-e80">
      <w lemma="28" orig="28ᵗʰ" pos="ord" xml:id="A82191-001-a-0380">28th</w>
      <pc unit="sentence" xml:id="A82191-001-a-0400">.</pc>
      <hi rend="hi" xml:id="A82191-e100">
       <w lemma="may" pos="nn1" xml:id="A82191-001-a-0410">May</w>
       <pc xml:id="A82191-001-a-0420">,</pc>
      </hi>
      <w lemma="1648." pos="crd" xml:id="A82191-001-a-0430">1648.</w>
      <pc unit="sentence" xml:id="A82191-001-a-0440"/>
     </date>
    </head>
    <opener xml:id="A82191-e110">
     <salute xml:id="A82191-e120">
      <w lemma="worshipful" pos="j" reg="Worshipful" xml:id="A82191-001-a-0450">Worshipfull</w>
      <pc xml:id="A82191-001-a-0460">;</pc>
     </salute>
    </opener>
    <p xml:id="A82191-e130">
     <w lemma="these" pos="d" rend="decorinit" xml:id="A82191-001-a-0470">THese</w>
     <w lemma="be" pos="vvb" xml:id="A82191-001-a-0480">are</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-0490">to</w>
     <w lemma="certify" pos="vvi" reg="certify" xml:id="A82191-001-a-0500">certifie</w>
     <w lemma="you" pos="pn" xml:id="A82191-001-a-0510">you</w>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-0520">that</w>
     <w lemma="we" pos="pns" reg="we" xml:id="A82191-001-a-0530">wee</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0540">the</w>
     <w lemma="commander" pos="n2" xml:id="A82191-001-a-0550">Commanders</w>
     <pc xml:id="A82191-001-a-0560">,</pc>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-0570">and</w>
     <w lemma="officer" pos="n2" xml:id="A82191-001-a-0580">Officers</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0590">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0600">the</w>
     <w lemma="ship" pos="n1" xml:id="A82191-001-a-0610">Ship</w>
     <w lemma="constant" pos="j" xml:id="A82191-001-a-0620">Constant</w>
     <w lemma="reformation" pos="n1" xml:id="A82191-001-a-0630">Reformation</w>
     <pc xml:id="A82191-001-a-0640">,</pc>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-0650">with</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0660">the</w>
     <w lemma="rest" pos="n1" xml:id="A82191-001-a-0670">rest</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0680">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0690">the</w>
     <w lemma="fleet" pos="n1" xml:id="A82191-001-a-0700">Fleet</w>
     <pc xml:id="A82191-001-a-0710">,</pc>
     <w lemma="have" pos="vvb" xml:id="A82191-001-a-0720">have</w>
     <w lemma="secure" pos="vvn" xml:id="A82191-001-a-0730">secured</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0740">the</w>
     <w lemma="ship" pos="n2" xml:id="A82191-001-a-0750">Ships</w>
     <w lemma="for" pos="acp" xml:id="A82191-001-a-0760">for</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0770">the</w>
     <w lemma="service" pos="n1" xml:id="A82191-001-a-0780">service</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0790">of</w>
     <w lemma="king" pos="n1" xml:id="A82191-001-a-0800">King</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-0810">and</w>
     <hi xml:id="A82191-e140">
      <w lemma="parliament" pos="n1" xml:id="A82191-001-a-0820">Parliament</w>
      <pc xml:id="A82191-001-a-0830">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-0840">and</w>
     <w lemma="have" pos="vvb" xml:id="A82191-001-a-0850">have</w>
     <w lemma="refuse" pos="vvn" xml:id="A82191-001-a-0860">refused</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-0870">to</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-0880">be</w>
     <w lemma="under" pos="acp" xml:id="A82191-001-a-0890">under</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-0900">the</w>
     <w lemma="command" pos="n1" xml:id="A82191-001-a-0910">Command</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-0920">of</w>
     <w lemma="colonel" pos="n1" reg="colonel" xml:id="A82191-001-a-0930">Colonell</w>
     <hi xml:id="A82191-e150">
      <w lemma="Rainsbrough" pos="nn1" xml:id="A82191-001-a-0940">Rainsbrough</w>
      <pc xml:id="A82191-001-a-0950">,</pc>
     </hi>
     <w lemma="by" pos="acp" xml:id="A82191-001-a-0960">by</w>
     <w lemma="reason" pos="n1" xml:id="A82191-001-a-0970">reason</w>
     <w lemma="we" pos="pns" reg="we" xml:id="A82191-001-a-0980">wee</w>
     <w lemma="conceive" pos="vvb" xml:id="A82191-001-a-0990">conceive</w>
     <w lemma="he" pos="pno" xml:id="A82191-001-a-1000">him</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-1010">to</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-1020">be</w>
     <w lemma="a" pos="d" xml:id="A82191-001-a-1030">a</w>
     <w lemma="man" pos="n1" xml:id="A82191-001-a-1040">man</w>
     <w lemma="not" pos="xx" xml:id="A82191-001-a-1050">not</w>
     <w lemma="well-affected" pos="j" reg="well-affected" xml:id="A82191-001-a-1060">wel-affected</w>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-1070">to</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1080">the</w>
     <w lemma="king" pos="n1" xml:id="A82191-001-a-1090">King</w>
     <pc xml:id="A82191-001-a-1100">,</pc>
     <hi xml:id="A82191-e160">
      <w lemma="parliament" pos="n1" xml:id="A82191-001-a-1110">Parliament</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-1120">and</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A82191-001-a-1130">Kingdome</w>
     <pc xml:id="A82191-001-a-1140">,</pc>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-1150">and</w>
     <w lemma="we" pos="pns" xml:id="A82191-001-a-1160">we</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A82191-001-a-1170">doe</w>
     <w lemma="hereby" pos="av" xml:id="A82191-001-a-1180">hereby</w>
     <w lemma="declare" pos="vvi" xml:id="A82191-001-a-1190">declare</w>
     <w lemma="unto" pos="acp" xml:id="A82191-001-a-1200">unto</w>
     <w lemma="you" pos="pn" xml:id="A82191-001-a-1210">you</w>
     <pc xml:id="A82191-001-a-1220">,</pc>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-1230">that</w>
     <w lemma="we" pos="pns" xml:id="A82191-001-a-1240">we</w>
     <w lemma="have" pos="vvb" xml:id="A82191-001-a-1250">have</w>
     <w lemma="unanimous" pos="av-j" xml:id="A82191-001-a-1260">unanimously</w>
     <w lemma="join" pos="vvn" reg="joined" xml:id="A82191-001-a-1270">joyned</w>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-1280">with</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1290">the</w>
     <hi xml:id="A82191-e170">
      <w lemma="kentish" pos="jnn" xml:id="A82191-001-a-1300">Kentish</w>
     </hi>
     <w lemma="gentleman" pos="n2" xml:id="A82191-001-a-1310">Gentlemen</w>
     <pc xml:id="A82191-001-a-1320">,</pc>
     <w lemma="in" pos="acp" xml:id="A82191-001-a-1330">in</w>
     <w lemma="their" pos="po" xml:id="A82191-001-a-1340">their</w>
     <w lemma="just" pos="j" xml:id="A82191-001-a-1350">just</w>
     <w lemma="petition" pos="n1" xml:id="A82191-001-a-1360">Petition</w>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-1370">to</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1380">the</w>
     <hi xml:id="A82191-e180">
      <w lemma="parliament" pos="n1" xml:id="A82191-001-a-1390">Parliament</w>
      <pc xml:id="A82191-001-a-1400">,</pc>
     </hi>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-1410">to</w>
     <w lemma="this" pos="d" xml:id="A82191-001-a-1420">this</w>
     <w lemma="purpose" pos="n1" xml:id="A82191-001-a-1430">purpose</w>
     <w lemma="follow" pos="vvg" xml:id="A82191-001-a-1440">following</w>
     <pc xml:id="A82191-001-a-1450">,</pc>
     <hi xml:id="A82191-e190">
      <w lemma="videlicet" pos="fla" xml:id="A82191-001-a-1460">videlicet</w>
      <pc unit="sentence" xml:id="A82191-001-a-1470">.</pc>
     </hi>
    </p>
    <p xml:id="A82191-e200">
     <w lemma="first" pos="ord" xml:id="A82191-001-a-1480">First</w>
     <pc xml:id="A82191-001-a-1490">,</pc>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-1500">that</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1510">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A82191-001-a-1520">Kings</w>
     <w lemma="majesty" pos="n1" xml:id="A82191-001-a-1530">Majesty</w>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-1540">with</w>
     <w lemma="all" pos="d" xml:id="A82191-001-a-1550">all</w>
     <w lemma="expedition" pos="n1" xml:id="A82191-001-a-1560">expedition</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-1570">be</w>
     <w lemma="admit" pos="vvn" xml:id="A82191-001-a-1580">admitted</w>
     <w lemma="in" pos="acp" xml:id="A82191-001-a-1590">in</w>
     <w lemma="safety" pos="n1" xml:id="A82191-001-a-1600">Safety</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-1610">and</w>
     <w lemma="honour" pos="n1" xml:id="A82191-001-a-1620">Honour</w>
     <pc xml:id="A82191-001-a-1630">,</pc>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-1640">to</w>
     <w lemma="treat" pos="vvi" xml:id="A82191-001-a-1650">treat</w>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-1660">with</w>
     <w lemma="his" pos="po" xml:id="A82191-001-a-1670">his</w>
     <w lemma="two" pos="crd" xml:id="A82191-001-a-1680">two</w>
     <w lemma="house" pos="n2" xml:id="A82191-001-a-1690">Houses</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-1700">of</w>
     <hi xml:id="A82191-e210">
      <w lemma="parliament" pos="n1" xml:id="A82191-001-a-1710">Parliament</w>
      <pc unit="sentence" xml:id="A82191-001-a-1720">.</pc>
     </hi>
    </p>
    <p xml:id="A82191-e220">
     <w lemma="second" pos="ord" xml:id="A82191-001-a-1730">Secondly</w>
     <pc xml:id="A82191-001-a-1740">,</pc>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-1750">that</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1760">the</w>
     <w lemma="army" pos="n1" xml:id="A82191-001-a-1770">Army</w>
     <w lemma="now" pos="av" xml:id="A82191-001-a-1780">now</w>
     <w lemma="under" pos="acp" xml:id="A82191-001-a-1790">under</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1800">the</w>
     <w lemma="command" pos="n1" xml:id="A82191-001-a-1810">Command</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-1820">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-1830">the</w>
     <w lemma="lord" pos="n1" xml:id="A82191-001-a-1840">Lord</w>
     <hi xml:id="A82191-e230">
      <w lemma="Fairfax" pos="nn1" xml:id="A82191-001-a-1850">Fairfax</w>
      <pc xml:id="A82191-001-a-1860">,</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-1870">to</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-1880">be</w>
     <w lemma="forthwith" pos="av" xml:id="A82191-001-a-1890">forthwith</w>
     <w lemma="disband" pos="vvd" xml:id="A82191-001-a-1900">disbanded</w>
     <pc xml:id="A82191-001-a-1910">,</pc>
     <w lemma="their" pos="po" xml:id="A82191-001-a-1920">their</w>
     <w lemma="arrear" pos="n2" xml:id="A82191-001-a-1930">Arrears</w>
     <w lemma="be" pos="vvg" xml:id="A82191-001-a-1940">being</w>
     <w lemma="pay" pos="vvn" xml:id="A82191-001-a-1950">paid</w>
     <w lemma="they" pos="pno" xml:id="A82191-001-a-1960">them</w>
     <pc unit="sentence" xml:id="A82191-001-a-1970">.</pc>
    </p>
    <p xml:id="A82191-e240">
     <w lemma="three" pos="ord" xml:id="A82191-001-a-1980">Thirdly</w>
     <pc xml:id="A82191-001-a-1990">,</pc>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-2000">That</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2010">the</w>
     <w lemma="know" pos="j-vn" xml:id="A82191-001-a-2020">known</w>
     <w lemma="law" pos="n2" xml:id="A82191-001-a-2030">Laws</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-2040">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2050">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A82191-001-a-2060">Kingdome</w>
     <w lemma="may" pos="vmb" xml:id="A82191-001-a-2070">may</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-2080">be</w>
     <w lemma="establish" pos="vvn" xml:id="A82191-001-a-2090">Established</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2100">and</w>
     <w lemma="continue" pos="vvn" xml:id="A82191-001-a-2110">continued</w>
     <pc xml:id="A82191-001-a-2120">,</pc>
     <w lemma="whereby" pos="crq" xml:id="A82191-001-a-2130">whereby</w>
     <w lemma="we" pos="pns" xml:id="A82191-001-a-2140">we</w>
     <w lemma="ought" pos="vmd" xml:id="A82191-001-a-2150">ought</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-2160">to</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-2170">be</w>
     <w lemma="govern" pos="vvn" xml:id="A82191-001-a-2180">Governed</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2190">and</w>
     <w lemma="judge" pos="vvn" reg="judged" xml:id="A82191-001-a-2200">Iudged</w>
     <pc unit="sentence" xml:id="A82191-001-a-2210">.</pc>
    </p>
    <p xml:id="A82191-e250">
     <w lemma="four" pos="ord" xml:id="A82191-001-a-2220">Fourthly</w>
     <pc xml:id="A82191-001-a-2230">,</pc>
     <w lemma="that" pos="cs" xml:id="A82191-001-a-2240">That</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2250">the</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A82191-001-a-2260">Priviledges</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-2270">of</w>
     <w lemma="parliament" pos="n1" xml:id="A82191-001-a-2280">Parliament</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2290">and</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2300">the</w>
     <w lemma="liberty" pos="n1" xml:id="A82191-001-a-2310">Liberty</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-2320">of</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2330">the</w>
     <w lemma="subject" pos="n2" xml:id="A82191-001-a-2340">Subjects</w>
     <w lemma="may" pos="vmb" xml:id="A82191-001-a-2350">may</w>
     <w lemma="be" pos="vvi" xml:id="A82191-001-a-2360">be</w>
     <w lemma="preserve" pos="vvn" xml:id="A82191-001-a-2370">preserved</w>
     <pc unit="sentence" xml:id="A82191-001-a-2380">.</pc>
    </p>
    <p xml:id="A82191-e260">
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2390">And</w>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-2400">to</w>
     <w lemma="this" pos="d" xml:id="A82191-001-a-2410">this</w>
     <w lemma="purpose" pos="n1" xml:id="A82191-001-a-2420">purpose</w>
     <w lemma="we" pos="pns" xml:id="A82191-001-a-2430">we</w>
     <w lemma="have" pos="vvb" xml:id="A82191-001-a-2440">have</w>
     <w lemma="send" pos="vvn" xml:id="A82191-001-a-2450">sent</w>
     <w lemma="our" pos="po" xml:id="A82191-001-a-2460">our</w>
     <w lemma="love" pos="j-vg" xml:id="A82191-001-a-2470">loving</w>
     <w lemma="friend" pos="n1" xml:id="A82191-001-a-2480">Friend</w>
     <w lemma="captain" pos="n1" reg="captain" xml:id="A82191-001-a-2490">Captaine</w>
     <hi xml:id="A82191-e270">
      <w lemma="penrose" pos="nn1" xml:id="A82191-001-a-2500">Penrose</w>
      <pc xml:id="A82191-001-a-2510">,</pc>
     </hi>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-2520">with</w>
     <w lemma="a" pos="d" xml:id="A82191-001-a-2530">a</w>
     <w lemma="letter" pos="n1" xml:id="A82191-001-a-2540">Letter</w>
     <w lemma="to" pos="acp" xml:id="A82191-001-a-2550">to</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2560">the</w>
     <w lemma="earl" pos="n1" reg="Earl" xml:id="A82191-001-a-2570">Earle</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-2580">of</w>
     <hi xml:id="A82191-e280">
      <w lemma="warwick" pos="nn1" xml:id="A82191-001-a-2590">Warwick</w>
      <pc xml:id="A82191-001-a-2600">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2610">and</w>
     <w lemma="we" pos="pns" xml:id="A82191-001-a-2620">we</w>
     <w lemma="be" pos="vvb" xml:id="A82191-001-a-2630">are</w>
     <w lemma="resolve" pos="vvn" xml:id="A82191-001-a-2640">resolved</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-2650">to</w>
     <w lemma="take" pos="vvi" xml:id="A82191-001-a-2660">take</w>
     <w lemma="in" pos="acp" xml:id="A82191-001-a-2670">in</w>
     <w lemma="no" pos="dx" xml:id="A82191-001-a-2680">no</w>
     <w lemma="commander" pos="n1" xml:id="A82191-001-a-2690">Commander</w>
     <w lemma="whatsoever" pos="crq" xml:id="A82191-001-a-2700">whatsoever</w>
     <pc xml:id="A82191-001-a-2710">,</pc>
     <w lemma="but" pos="acp" xml:id="A82191-001-a-2720">but</w>
     <w lemma="such" pos="d" xml:id="A82191-001-a-2730">such</w>
     <w lemma="as" pos="acp" xml:id="A82191-001-a-2740">as</w>
     <w lemma="shall" pos="vmb" xml:id="A82191-001-a-2750">shall</w>
     <w lemma="agree" pos="vvi" xml:id="A82191-001-a-2760">agree</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2770">and</w>
     <w lemma="correspond" pos="vvi" xml:id="A82191-001-a-2780">correspond</w>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-2790">with</w>
     <w lemma="we" pos="pno" xml:id="A82191-001-a-2800">us</w>
     <w lemma="in" pos="acp" xml:id="A82191-001-a-2810">in</w>
     <w lemma="this" pos="d" xml:id="A82191-001-a-2820">this</w>
     <w lemma="petition" pos="n1" xml:id="A82191-001-a-2830">Petition</w>
     <pc xml:id="A82191-001-a-2840">,</pc>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2850">and</w>
     <w lemma="shall" pos="vmb" xml:id="A82191-001-a-2860">shall</w>
     <w lemma="resolve" pos="vvi" xml:id="A82191-001-a-2870">resolve</w>
     <w lemma="to" pos="prt" xml:id="A82191-001-a-2880">to</w>
     <w lemma="live" pos="vvi" xml:id="A82191-001-a-2890">live</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-2900">and</w>
     <w lemma="die" pos="vvi" reg="die" xml:id="A82191-001-a-2910">dye</w>
     <w lemma="with" pos="acp" xml:id="A82191-001-a-2920">with</w>
     <w lemma="we" pos="pno" xml:id="A82191-001-a-2930">us</w>
     <pc xml:id="A82191-001-a-2940">,</pc>
     <w lemma="in" pos="acp" xml:id="A82191-001-a-2950">in</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-2960">the</w>
     <w lemma="behalf" pos="n1" reg="behalf" xml:id="A82191-001-a-2970">behalfe</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-2980">of</w>
     <w lemma="king" pos="n1" xml:id="A82191-001-a-2990">King</w>
     <w lemma="and" pos="cc" xml:id="A82191-001-a-3000">and</w>
     <w lemma="parliament" pos="n1" xml:id="A82191-001-a-3010">Parliament</w>
     <pc xml:id="A82191-001-a-3020">,</pc>
     <w lemma="which" pos="crq" xml:id="A82191-001-a-3030">which</w>
     <w lemma="be" pos="vvz" xml:id="A82191-001-a-3040">is</w>
     <w lemma="the" pos="d" xml:id="A82191-001-a-3050">the</w>
     <w lemma="positive" pos="j" xml:id="A82191-001-a-3060">Positive</w>
     <w lemma="result" pos="n1" xml:id="A82191-001-a-3070">Result</w>
     <w lemma="of" pos="acp" xml:id="A82191-001-a-3080">of</w>
     <w lemma="we" pos="pno" xml:id="A82191-001-a-3090">us</w>
     <pc unit="sentence" xml:id="A82191-001-a-3100">.</pc>
    </p>
    <closer xml:id="A82191-e290">
     <w lemma="we" pos="pns" xml:id="A82191-001-a-3110">We</w>
     <w lemma="humble" pos="av-j" xml:id="A82191-001-a-3120">humbly</w>
     <w lemma="desire" pos="vvb" xml:id="A82191-001-a-3130">desire</w>
     <w lemma="your" pos="po" xml:id="A82191-001-a-3140">your</w>
     <w lemma="speedy" pos="j" xml:id="A82191-001-a-3150">speedy</w>
     <w lemma="answer" pos="n1" xml:id="A82191-001-a-3160">Answer</w>
     <pc unit="sentence" xml:id="A82191-001-a-3170">.</pc>
     <signed xml:id="A82191-e300">
      <list xml:id="A82191-e310">
       <head xml:id="A82191-e320">
        <w lemma="officer" pos="n2" xml:id="A82191-001-a-3180">Officers</w>
        <w lemma="of" pos="acp" xml:id="A82191-001-a-3190">of</w>
        <w lemma="the" pos="d" xml:id="A82191-001-a-3200">the</w>
        <w lemma="constant" pos="j" xml:id="A82191-001-a-3210">constant</w>
        <w lemma="reformation" pos="n1" xml:id="A82191-001-a-3220">Reformation</w>
        <pc unit="sentence" xml:id="A82191-001-a-3230">.</pc>
       </head>
       <item xml:id="A82191-e330">
        <hi xml:id="A82191-e340">
         <w lemma="Thomas" pos="nn1" xml:id="A82191-001-a-3240">Thomas</w>
         <w lemma="Lisle" pos="nn1" xml:id="A82191-001-a-3250">Lisle</w>
         <pc xml:id="A82191-001-a-3260">,</pc>
        </hi>
        <w lemma="lieutenant" pos="n1" reg="lieutenant" xml:id="A82191-001-a-3270">Lievetenant</w>
        <pc unit="sentence" xml:id="A82191-001-a-3280">.</pc>
       </item>
       <item xml:id="A82191-e350">
        <hi xml:id="A82191-e360">
         <w lemma="Andrew" pos="nn1" xml:id="A82191-001-a-3290">Andrew</w>
         <w lemma="mitchel" pos="nn1" xml:id="A82191-001-a-3300">Mitchel</w>
        </hi>
        <w lemma="boat" pos="n2" xml:id="A82191-001-a-3310">Boats</w>
        <pc unit="sentence" xml:id="A82191-001-a-3320">.</pc>
       </item>
       <item xml:id="A82191-e370">
        <hi xml:id="A82191-e380">
         <w lemma="James" pos="nn1" xml:id="A82191-001-a-3330">James</w>
         <w lemma="Allen" pos="nn1" xml:id="A82191-001-a-3340">Allen</w>
         <pc xml:id="A82191-001-a-3350">,</pc>
        </hi>
        <w lemma="gunner" pos="n1" xml:id="A82191-001-a-3360">Gunner</w>
        <pc unit="sentence" xml:id="A82191-001-a-3370">.</pc>
       </item>
       <item xml:id="A82191-e390">
        <hi xml:id="A82191-e400">
         <w lemma="tho." pos="ab" xml:id="A82191-001-a-3380">Tho.</w>
         <w lemma="best" pos="zz" xml:id="A82191-001-a-3390">Best</w>
         <pc xml:id="A82191-001-a-3400">,</pc>
        </hi>
        <w lemma="carpenter" pos="n1" xml:id="A82191-001-a-3410">Carpenter</w>
        <pc unit="sentence" xml:id="A82191-001-a-3420">.</pc>
       </item>
      </list>
      <list xml:id="A82191-e410">
       <head xml:id="A82191-e420">
        <w lemma="officer" pos="n2" xml:id="A82191-001-a-3430">Officers</w>
        <w lemma="of" pos="acp" xml:id="A82191-001-a-3440">of</w>
        <w lemma="the" pos="d" xml:id="A82191-001-a-3450">the</w>
        <hi xml:id="A82191-e430">
         <w lemma="swallow" pos="n1" xml:id="A82191-001-a-3460">Swallow</w>
         <pc unit="sentence" xml:id="A82191-001-a-3470">.</pc>
        </hi>
       </head>
       <item xml:id="A82191-e440">
        <hi xml:id="A82191-e450">
         <w lemma="Leonard" pos="nn1" xml:id="A82191-001-a-3480">Leonard</w>
         <w lemma="Harris" pos="nn1" xml:id="A82191-001-a-3490">Harris</w>
         <pc xml:id="A82191-001-a-3500">,</pc>
        </hi>
        <w lemma="capt." pos="ab" xml:id="A82191-001-a-3510">Capt.</w>
        <pc unit="sentence" xml:id="A82191-001-a-3520"/>
       </item>
       <item xml:id="A82191-e460">
        <hi xml:id="A82191-e470">
         <w lemma="jo." pos="ab" xml:id="A82191-001-a-3530">Jo.</w>
         <w lemma="London" pos="nn1" xml:id="A82191-001-a-3540">London</w>
         <pc xml:id="A82191-001-a-3550">,</pc>
        </hi>
        <w lemma="mr." pos="ab" xml:id="A82191-001-a-3560">Mr.</w>
        <pc unit="sentence" xml:id="A82191-001-a-3570"/>
       </item>
       <item xml:id="A82191-e480">
        <hi xml:id="A82191-e490">
         <w lemma="nich." pos="ab" xml:id="A82191-001-a-3580">Nich.</w>
         <w lemma="Laurence" pos="nn1" xml:id="A82191-001-a-3590">Laurence</w>
         <pc xml:id="A82191-001-a-3600">,</pc>
        </hi>
        <w lemma="Lievet" pos="nn1" xml:id="A82191-001-a-3610">Lievet</w>
        <pc unit="sentence" xml:id="A82191-001-a-3620">.</pc>
       </item>
       <item xml:id="A82191-e500">
        <hi xml:id="A82191-e510">
         <w lemma="andr." pos="ab" xml:id="A82191-001-a-3630">Andr.</w>
         <w lemma="jackson" pos="nn1" reg="Jackson" xml:id="A82191-001-a-3640">Iackson</w>
         <pc xml:id="A82191-001-a-3650">,</pc>
        </hi>
        <w lemma="gunner" pos="n1" xml:id="A82191-001-a-3660">Gunner</w>
        <pc unit="sentence" xml:id="A82191-001-a-3670">.</pc>
       </item>
       <item xml:id="A82191-e520">
        <hi xml:id="A82191-e530">
         <w lemma="io." pos="ab" xml:id="A82191-001-a-3680">Io.</w>
         <w lemma="short" pos="nn1" xml:id="A82191-001-a-3690">Short</w>
         <pc xml:id="A82191-001-a-3700">,</pc>
        </hi>
        <w lemma="carpenter" pos="n1" xml:id="A82191-001-a-3710">Carpenter</w>
        <pc unit="sentence" xml:id="A82191-001-a-3720">.</pc>
       </item>
      </list>
      <w lemma="sign" pos="vvn" xml:id="A82191-001-a-3730">Signed</w>
      <w lemma="likewise" pos="av" xml:id="A82191-001-a-3740">likewise</w>
      <w lemma="by" pos="acp" xml:id="A82191-001-a-3750">by</w>
      <w lemma="the" pos="d" xml:id="A82191-001-a-3760">the</w>
      <w lemma="captain" pos="n1" reg="captain" xml:id="A82191-001-a-3770">Captaine</w>
      <w lemma="of" pos="acp" xml:id="A82191-001-a-3780">of</w>
      <w lemma="the" pos="d" xml:id="A82191-001-a-3790">the</w>
      <hi xml:id="A82191-e540">
       <w lemma="roebuck" pos="n1" xml:id="A82191-001-a-3800">Roebuck</w>
       <pc xml:id="A82191-001-a-3810">,</pc>
       <w lemma="hind" pos="n1" reg="hind" xml:id="A82191-001-a-3820">Hinde</w>
       <pc xml:id="A82191-001-a-3830">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A82191-001-a-3840">and</w>
      <w lemma="several" pos="j" reg="several" xml:id="A82191-001-a-3850">severall</w>
      <w lemma="other" pos="d" xml:id="A82191-001-a-3860">other</w>
      <w lemma="officer" pos="n2" xml:id="A82191-001-a-3870">Officers</w>
      <w lemma="of" pos="acp" xml:id="A82191-001-a-3880">of</w>
      <w lemma="these" pos="d" xml:id="A82191-001-a-3890">these</w>
      <w lemma="and" pos="cc" xml:id="A82191-001-a-3900">and</w>
      <w lemma="other" pos="d" xml:id="A82191-001-a-3910">other</w>
      <w lemma="ship" pos="n2" xml:id="A82191-001-a-3920">Ships</w>
      <pc unit="sentence" xml:id="A82191-001-a-3930">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
